# Create Vpc

resource "aws_vpc" "vpc" {
  cidr_block = var.block

  tags = {
    Name = "ninja-vpc-01"
  }
}

# Create Public Subnet

resource "aws_subnet" "public_subnet" {
  count             = length(var.public_subnet_cidr)
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = element(var.public_subnet_cidr, count.index)
  availability_zone = element(var.azs, count.index)
  tags = {
    Name = "ninja-pub-subnet-${count.index + 1}"

  }
}

# Create Private Subnet

resource "aws_subnet" "private_subnet" {
  count             = length(var.private_subnet_cidr)
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = element(var.private_subnet_cidr, count.index)
  availability_zone = element(var.azs, count.index)

  tags = {
    Name = "ninja-priv-subnet-${count.index + 1}"
  }
}

#Create InternetGateway

resource "aws_internet_gateway" "ig" {
  vpc_id = aws_vpc.vpc.id
  tags = {
    Name = "ninja-igw-01"
  }
}

# Create ElasticIP

resource "aws_eip" "elasticip" {

  vpc        = true
  depends_on = [aws_internet_gateway.ig]
}

# Create Nat Gateway

resource "aws_nat_gateway" "nat" {
  allocation_id = aws_eip.elasticip.id
  subnet_id     = element(aws_subnet.public_subnet.*.id, 0)
  depends_on    = [aws_internet_gateway.ig]

  tags = {
    Name = "ninja-nat-01"
  }
}

# Create PUblic Route Table

resource "aws_route_table" "public-rt" {
  vpc_id = aws_vpc.vpc.id
  tags = {
    Name = "ninja-route-pub-01"
  }
}

# Create Private Route Table
resource "aws_route_table" "private-rt" {
  vpc_id = aws_vpc.vpc.id
  tags = {
    Name = "ninja-route-priv-01"
  }
}

# Route igw in route table

resource "aws_route" "public-route" {
  route_table_id         = aws_route_table.public-rt.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.ig.id
}

# Route Nat in route table

resource "aws_route" "private-route" {
  route_table_id         = aws_route_table.private-rt.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.nat.id
}

# Associate route table with public subnet

resource "aws_route_table_association" "public-association" {
  count          = length(var.public_subnet_cidr)
  subnet_id      = element(aws_subnet.public_subnet.*.id, count.index)
  route_table_id = aws_route_table.public-rt.id
}

# Associate Route table with private subnet
resource "aws_route_table_association" "private-association" {
  count          = length(var.private_subnet_cidr)
  subnet_id      = element(aws_subnet.private_subnet.*.id, count.index)
  route_table_id = aws_route_table.private-rt.id
}
