terraform {
  backend "s3" {
    bucket = "terraformtaskbucket1"
    key    = "kafka_project/terraform.tfstate"
    region = "us-west-1"
  }
}
